FROM openjdk:8-jdk-alpine

WORKDIR /usr/src/app

COPY ./ ./

CMD [ "java", "-jar", "spring-boot-web-application-annotation_tool-1.0.war"]
